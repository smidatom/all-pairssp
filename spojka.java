import java.util.PriorityQueue;
import java.util.Queue;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class spojka
{
	static class Node implements Comparable<Node>
	{
		public Integer name;
		public ArrayList<Node> successors;
		public ArrayList<Integer> distances;
		public int value;
		public boolean open;
		public int status;
		public static final int FRESH = 0;
		public static final int OPEN = 1;
		public static final int CLOSED = 2;


		public Node(Integer name)
		{
			this.name = name;
			value = Integer.MAX_VALUE;
			open = true;
			successors = new ArrayList<Node>();
			distances = new ArrayList<Integer>();
			status = FRESH;
		}

		public void addSucc(Node succ)
		{
			successors.add(succ);
			return;
		}

		public void addDistance(Integer distance)
		{
			distances.add(distance);
			return;
		}

		public int compareTo(Node other)
		{
			return this.value - other.value;
		}
	}

	public static void printGraph(HashMap<Integer, Node> graph)
	{
		for(Map.Entry<Integer, Node> entry : graph.entrySet())
		{
		    Integer key = entry.getKey();
		    Node value = entry.getValue();
		    System.out.println(key);
		    System.out.print("Val> " + value.value + " ");
		    System.out.print("Succ> ");
		    for(Node scc : value.successors)
		    {
		    	System.out.print(scc.value + " ");
		    }
		    System.out.println();
		}
	}


	public static void resetGraph(HashMap<Integer, Node> graph)
	{
		for(Map.Entry<Integer, Node> entry : graph.entrySet())
		{
			Node value = entry.getValue();
			value.open = true;
			value.value = Integer.MAX_VALUE;
			value.status = Node.FRESH;
		}
		return;
	}

	public static boolean testConectivity(HashMap<Integer, Node> graph)
	{
		Node start = graph.entrySet().iterator().next().getValue();
		Node tmp;
		Queue<Node> queue = new LinkedList<Node>();
		queue.offer(start);
		while(!queue.isEmpty())
		{
			tmp = queue.poll();
			for(Node succ : tmp.successors)
			{
				if(succ.status == Node.FRESH)
				{
					succ.status = Node.OPEN;
					queue.offer(succ);
				}
			}
			tmp.status = Node.CLOSED;
		}

		for(Map.Entry<Integer, Node> entry : graph.entrySet())
		{
			tmp = entry.getValue();
			if(tmp.status==Node.FRESH)
				return false;
		}
		
		resetGraph(graph);
		return true;
	}

	public static int dijkstra(HashMap<Integer, Node> graph, Node start)
	{
		PriorityQueue<Node> queue = new PriorityQueue<Node>();
		Node neigh, tmp;
		int newVal, neighDist, maxVal = Integer.MIN_VALUE;

		start.value = 0;
		queue.offer(start);
		while(!queue.isEmpty())
		{
			tmp = queue.poll();
			tmp.open = false;
			for(int i=0; i<tmp.successors.size(); i++)
			{
				neigh = tmp.successors.get(Integer.valueOf(i));
				neighDist = tmp.distances.get(Integer.valueOf(i));
				if(neigh.open)
				{
					newVal = tmp.value + neighDist;
					if(neigh.value > newVal)
					{
						queue.remove(neigh);
						neigh.value = newVal;
						queue.offer(neigh);
					}
				}
			}
		}
		for(Map.Entry<Integer, Node> entry : graph.entrySet())
		{
			tmp = entry.getValue();
			if(maxVal<tmp.value)
				maxVal = tmp.value;
		}

		resetGraph(graph);
		return maxVal;
	}

	public static void solveProblem() throws Exception
	{	
		HashMap<Integer, Node> graph = new HashMap<Integer, Node>();
		Node tmp;
		int nodes, edges, maxVal = Integer.MIN_VALUE, rec;
		Integer from, to, distance;
		nodes = nextInt();
		edges = nextInt();
		for(int i=0; i<edges; i++)
		{
			from = nextInt();
			to = nextInt();
			distance = nextInt();
			if(!graph.containsKey(from))
				graph.put(from, new Node(from));
			if(!graph.containsKey(to))
				graph.put(to, new Node(to));

			tmp = graph.get(from);
			tmp.addSucc(graph.get(to));
			tmp.addDistance(distance);
			tmp = graph.get(to);
			tmp.addSucc(graph.get(from));
			tmp.addDistance(distance);
		}	

		if(graph.size()==nodes && testConectivity(graph))
		{
			for(Map.Entry<Integer, Node> entry : graph.entrySet())
			{
				rec = dijkstra(graph, entry.getValue());
				if(maxVal < rec)
					maxVal = rec;
			}
			System.out.println("Nejvetsi vzdalenost je " + maxVal + ".");
		}
		else
			System.out.println("Bez spojeni neni veleni!");
		
		return;
	}

	public static void main(String[] args) throws Exception
	{
		int cases = nextInt();
		for(int i=0; i<cases; i++)
			solveProblem();
		return;
	}

	public static String nextToken() throws Exception
	{
		while (!st.hasMoreTokens())
		st = new StringTokenizer(input.readLine());
		return st.nextToken();
	}

	public static int nextInt() throws Exception
	{
		return Integer.parseInt(nextToken());
	}

	static StringTokenizer st = new StringTokenizer("");
	static BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
}